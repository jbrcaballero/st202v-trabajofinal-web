package edu.uni.st202v.tfinal.util;

/**
 * Created by JC on 5/18/2017.
 */

public class St202vUtil {
	
	
	public static final String RUTA_RELATIVA_DLLS = "../../src/main/resources/lib/";
	public static final String RUTA_RELATIVA_ARCHIVOS = "../../src/main/resources/data/";
	
	public static final String NOMBRE_DLL_ARCHIVOS = "libst202v-final-libreria.dll";
	
	public static final String NOMBRE_ARCHIVO_PRODUCTOS = "productos.txt";
	
	public static String RUTA_DLLS = "";
	public static String RUTA_ARCHIVOS = "";
	
	public static void inicializarParametros() throws Exception{
		
		String rutaRaiz = "";
		try{
			rutaRaiz = St202vUtil.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();	
		}catch(Exception e){
			throw new Exception("Error al ubicar ruta de librerias y archivos");
		}

		rutaRaiz = rutaRaiz.substring(1,rutaRaiz.length());

		RUTA_DLLS = rutaRaiz+RUTA_RELATIVA_DLLS;
		RUTA_ARCHIVOS = rutaRaiz+RUTA_RELATIVA_ARCHIVOS;
		
	}

}
