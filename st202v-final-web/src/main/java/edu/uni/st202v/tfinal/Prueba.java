package edu.uni.st202v.tfinal;

/**
 * Created by JC on 5/18/2017.
 */

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Structure;

import edu.uni.st202v.tfinal.util.St202vUtil;

import java.util.Arrays;
import java.util.List;



public class Prueba {

    // This is the standard, stable way of mapping, which supports extensive
    // customization and mapping of Java to native types.

	
	/**
	
	static String dllPath;
	
	public Prueba(String path){
		dllPath = path;
	}
	
	
    public interface DllInterface extends Library {
    	
   	
        DllInterface INSTANCE = (DllInterface) Native.loadLibrary(dllPath+"libst202v-final-libreria.dll", DllInterface.class);
        void test_f();
        miEstruct.ByValue getPersona();
        miEstruct.ByValue imprimirPersona(miEstruct.ByValue e);
        miEstruct.ByValue imprimirPersona1(miEstruct.ByValue e);
        
        Producto.ByValue agregarProducto(String nombreArchivo, Producto.ByValue prod);
        //Producto.ByValue agregarProducto(byte[] nombreArchivo);

    }

    public static class miEstruct extends Structure{
        public static class ByValue extends miEstruct implements Structure.ByValue {}
        public String nombre;
        public String apellido;
        public int edad;
        public float salario;
        
        @Override
        protected List<String> getFieldOrder()
        {
            //return Arrays.asList(new String[] { "nombre", "apellido"});
            return Arrays.asList(new String[] { "nombre", "apellido","edad","salario" });
        }
    }
    
    
    public static class Producto extends Structure{
    	public static class ByValue extends Producto implements Structure.ByValue{}
    	public String codigo;
    	public String descripcion;
        @Override
        protected List<String> getFieldOrder()
        {        	
            //return Arrays.asList(new String[] { "nombre", "apellido"});
            return Arrays.asList(new String[] { "codigo", "descripcion" });
        }
    	
    }
    
    
    

    void test_f() {
        DllInterface.INSTANCE.test_f();
    }


    void q(){
        miEstruct.ByValue w= new miEstruct.ByValue();
        w.nombre="Eduardox";
        w.apellido="Shenguix";
        w.edad=54;
        w.salario=(float)850.33;


        miEstruct.ByValue p = DllInterface.INSTANCE.imprimirPersona1(w);
        System.out.println(p.nombre);
        System.out.println(p.apellido);
        System.out.println(p.edad);
        System.out.println(p.salario);

    }



    void h(){
        System.out.println("zzzz");
        miEstruct.ByValue g = DllInterface.INSTANCE.getPersona();
        System.out.println(g.apellido);
        System.out.println(g.nombre);
        System.out.println("ppp");
        System.out.println(g.salario);
        System.out.println(g.edad);
    }


    void p(){
        miEstruct.ByValue w= new miEstruct.ByValue();
        w.nombre="Eduardo";
        w.apellido="Shengui";
        w.edad=54;
        w.salario=(float)850.33;


        miEstruct.ByValue p = DllInterface.INSTANCE.imprimirPersona(w);
        System.out.println(p.nombre);
        System.out.println(p.apellido);
        System.out.println(p.edad);
        System.out.println(p.salario);

    }
    
    public void w(){
    	Producto.ByValue prod = new Producto.ByValue();  	

    	prod.descripcion = "ssdfjlsdkjflds ";
    	prod.codigo = "123 ";
    
    	String nombreArchivo = St202vUtil.RUTA_ARCHIVOS+St202vUtil.NOMBRE_ARCHIVO_PRODUCTOS;
    	DllInterface.INSTANCE.agregarProducto(nombreArchivo,prod);
 
    }
    
    public void w(edu.uni.st202v.tfinal.entity.Producto producto){
    	Producto.ByValue prod = new Producto.ByValue();  	

    	prod.descripcion = producto.getDescripcion();
    	prod.codigo = producto.getCodigo();
    
    	String nombreArchivo = St202vUtil.RUTA_ARCHIVOS+St202vUtil.NOMBRE_ARCHIVO_PRODUCTOS;
    	try{
    		DllInterface.INSTANCE.agregarProducto(nombreArchivo,prod);	
    	}catch(Throwable e){
    		System.out.println("[TODO] Corregir Error Codigo C++");
    	}
    	
    }
**/
}
