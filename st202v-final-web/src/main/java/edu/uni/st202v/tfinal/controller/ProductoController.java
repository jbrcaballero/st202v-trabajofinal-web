package edu.uni.st202v.tfinal.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import edu.uni.st202v.tfinal.entity.Producto;
import edu.uni.st202v.tfinal.service.ProductoService; 

@Controller
@RequestMapping("/registrarProducto")
public class ProductoController {
	
	@GetMapping
	public ResponseEntity<?> agregarProducto1(){
		System.out.println(">>>>");
		return ResponseEntity.noContent().build();
	} 
	
	@PostMapping
	public String agregarProducto(@RequestParam String codigo, @RequestParam String descripcion){
		System.out.println(">>>>");
		System.out.println(codigo);
		System.out.println(descripcion);
		
		Producto producto = new Producto();
		producto.setCodigo(codigo);
		producto.setDescripcion(descripcion);
		
		ProductoService productoService = new ProductoService();
		productoService.agregarProducto(producto);
		
		
		return "redirect:/confirmacion.html";
	}
}

