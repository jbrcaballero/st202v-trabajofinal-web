package edu.uni.st202v.tfinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import edu.uni.st202v.tfinal.util.St202vUtil;

@SpringBootApplication
public class St202vFinal {

	public static void main(String[] args) throws Exception {
		
		St202vUtil.inicializarParametros();
		SpringApplication.run(St202vFinal.class, args);    
		
	}
}

		