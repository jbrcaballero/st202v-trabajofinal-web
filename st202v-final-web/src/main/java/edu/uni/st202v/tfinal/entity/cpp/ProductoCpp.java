package edu.uni.st202v.tfinal.entity.cpp;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Structure;

import edu.uni.st202v.tfinal.entity.Producto;
import edu.uni.st202v.tfinal.util.St202vUtil;

public class ProductoCpp {

	public static class ProductoCppDatos extends Structure {
		public static class ByValue extends ProductoCppDatos implements Structure.ByValue {
		}

		public String codigo;
		public String descripcion;

		@Override
		protected List<String> getFieldOrder() {
			return Arrays.asList(new String[] { "codigo", "descripcion" });
		}
	}

	public interface ProductoCPPFunciones extends Library {
		ProductoCPPFunciones INSTANCE = (ProductoCPPFunciones) Native
				.loadLibrary(St202vUtil.RUTA_DLLS + St202vUtil.NOMBRE_DLL_ARCHIVOS, ProductoCPPFunciones.class);

		void agregarProducto(String nombreArchivo, ProductoCppDatos.ByValue prod);
	}

	public void agregarProducto(Producto prod) {
		ProductoCppDatos.ByValue pd = new ProductoCppDatos.ByValue();
		pd.codigo = prod.getCodigo();
		pd.descripcion = prod.getDescripcion();
		ProductoCPPFunciones.INSTANCE.agregarProducto(St202vUtil.RUTA_ARCHIVOS+St202vUtil.NOMBRE_ARCHIVO_PRODUCTOS, pd);
	}

}
