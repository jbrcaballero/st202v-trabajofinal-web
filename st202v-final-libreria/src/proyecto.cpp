#include "proyecto.h"
#include <iostream>
#include <fstream>
using namespace std;


void test_f(){
	cout<<"Hello world";

}

struct Persona getPersona(){
	struct Persona p = {"Juan","Perez",5,405.55};
	return p;
}

struct Persona imprimirPersona(struct Persona p){
    p.nombre="ppxx";
    p.apellido="qqxx";
	p.edad++;
	p.salario+=1000;
	return p;
}


struct Persona imprimirPersona1(struct Persona p){
	p.nombre = string(p.nombre+"12");
	p.apellido = string(p.apellido+"2");
	p.edad++;
	p.salario+=1000;
	return p;
}

void agregarProducto(char *nombreArchivo, struct Producto prod){
	ofstream archivo(nombreArchivo,ios::app);
	if(archivo.is_open()){
		cout<<"Escribiendo archivo ...";
		archivo<<prod.codigo<<","<<prod.descripcion<<endl;
	}else{
		cout<<"Error accediendo al archivo..."<<endl;
	}
	archivo.close();

}
