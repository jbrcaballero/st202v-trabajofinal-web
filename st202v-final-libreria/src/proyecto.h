/*
 * proyecto.h
 *
 *  Created on: May 17, 2017
 *      Author: JC
 */

#ifndef PROYECTO_H_
#define PROYECTO_H_
#include<string>
using namespace std;
struct Persona{
	string nombre;
	//char apellido[100];
	string apellido;
	int edad;
	float salario;
};

struct Producto{
	char *codigo;
	char *descripcion;
};

extern "C"{
void test_f();
struct Persona getPersona();
struct Persona imprimirPersona(struct Persona p);
struct Persona imprimirPersona1(struct Persona p);
void agregarProducto(char* nombreArchivo, struct Producto prod);
}



#endif /* PROYECTO_H_ */
